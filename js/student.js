var jsonInfo = [{
    "name":"employees",
    "employees":[
        {"firstName":"John", "lastName":"Doe"}, 
        {"firstName":"Anna", "lastName":"Smith"},
        {"firstName":"Peter", "lastName":"Jones"}
    ]
    },
    {
    "name":"students",
    "students":[
        {"firstName":"John", "lastName":"Doe", "phone":"9000000000", "email":"test@gmail.com", "status":"active"}, 
        {"firstName":"Anna", "lastName":"Smith", "phone":"9000000000", "email":"test@gmail.com", "status":"inactive"},
        {"firstName":"Peter", "lastName":"Jones", "phone":"9000000000", "email":"test@gmail.com", "status":"active"},
        {"firstName":"John", "lastName":"Doe", "phone":"9000000000", "email":"test@gmail.com", "status":"active"}, 
        {"firstName":"Anna", "lastName":"Smith", "phone":"9000000000", "email":"test@gmail.com", "status":"active"},
        {"firstName":"Peter", "lastName":"Jones", "phone":"9000000000", "email":"test@gmail.com", "status":"active"},
        {"firstName":"John", "lastName":"Doe", "phone":"9000000000", "email":"test@gmail.com", "status":"active"}, 
        {"firstName":"Anna", "lastName":"Smith", "phone":"9000000000", "email":"test@gmail.com", "status":"inactive"},
        {"firstName":"Peter", "lastName":"Jones", "phone":"9000000000", "email":"test@gmail.com", "status":"active"}
    ]
    }];

var objs = jsonInfo[1].students;

for(var i = 0; i < objs.length; i++){
    json2table(i+1, objs[i]);
}

var form = document.getElementById("addInfoForm");
form.addEventListener("submit", function () {
    submitFormData();
});

function json2table(id, jsonObj) {
    var line = document.createElement("tr");
    line.appendChild(infoCell(id));

    var fullName = jsonObj.firstName + ' ' + jsonObj.lastName;
    line.appendChild(infoCell(fullName));

    line.appendChild(infoCell(jsonObj.email));
    line.appendChild(infoCell(jsonObj.phone));

    var actStatus = document.createElement("div");
    if (jsonObj.status == "active") {
        actStatus.className = "active";
    }else {
        actStatus.className = "inactive";
    }
    var statusTd = document.createElement("td");
    statusTd.appendChild(actStatus);
    line.appendChild(statusTd);

    var editInfo = document.createElement("button");
    editInfo.className = "btn-edit";
    editInfo.textContent = "Edit";
    editInfo.onclick = function (){editContent(this);};
    var editLine = document.createElement("td");
    editLine.appendChild(editInfo);
    line.appendChild(editLine);

    var deleteInfo = document.createElement("button");
    deleteInfo.className = "btn-delete";
    deleteInfo.textContent = "Delete";
    deleteInfo.onclick = function (){deleteContent(this);};
    var deleteLine = document.createElement("td");
    deleteLine.appendChild(deleteInfo);
    line.appendChild(deleteLine);

    var tableBody = document.getElementById("studentInfo");
    tableBody.appendChild(line);
}

function infoCell(val) {
    var cell = document.createElement("td");
    cell.textContent = val;
    return cell;
}

function editContent(Obj){
    alert(Obj);
}

function deleteContent(Obj){
    Obj.parentNode.parentNode.style.display="none";
}

function addInfo() {
    document.getElementById('background').style.display = "block";
}
function closeForm() {
    document.getElementById('background').style.display = "none";
}

function submitFormData(){
    var formData = new FormData(form);

    var f_id = formData.get("id");
    var f_firstname = formData.get("first-name");
    var f_lastname = formData.get("last-name");
    var f_email = formData.get("email");
    var f_phone = formData.get("phone");
    var f_status = formData.get("status");

    var o_json = info2json(f_firstname,f_lastname,f_email,f_phone,f_status);
    console.log(o_json);
    objs.push(o_json);
    alert("Added");
}

function info2json(i_firstname, i_lastname, i_email, i_phone, i_status) {
    var j_info = {};
    j_info.firstName = i_firstname;
    j_info.lastName = i_lastname;
    j_info.phone = i_phone;
    j_info.email = i_email;
    j_info.status = i_status;
    return j_info;
}
